<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Documento que muestra valores de variables</title>
</head>
<body>
    Recibimos el valor de la variable nombre:
    <br>
    <br>
    <?php
        if (isset($_GET['nombre']))
            echo "Nombre: " . $_GET['nombre'] . "<br>";
        
        if (isset($_GET['apellido'])) {
            echo "Apellido: " . $_GET['apellido'] . "<br>";
        }
        
        if (isset($_GET['edad']))
            echo "Edad: " . $_GET['edad'] . "<br>";
    ?>
</body>
</html>