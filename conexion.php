<?php
    $servername = "localhost";
    $database = "databasename";
    $username = "username";
    $password = "password";
    // Crear conexion
    $conn = mysqli_connect($servername, $username, $password, $database);
    
    // Chequear conexion
    if (!$conn) {
        die("Fallo la conexion: " . mysqli_connect_error());
    }
    echo "Conectada";
    //Cerrar la conexion
    //mysqli_close($conn);
?>