<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Links con variables</title>
</head>
<body>

    <a href="recibe.php?nombre=Pepe">Este es el link de Pepe</a>
    <a href="recibe.php?nombre=Julian&apellido=Lopez&edad=25">Este es el link de Julian</a>
    <a href="recibe.php?nombre=Mario&edad=31">Este es el link de Mario</a>

</body>
</html>