//Conexión
<?php
session_start();
$servername = "localhost";
$username   = "root";
$password   = "";
$dbname     = "tarde";
// Crear una conexión
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Verificar la conexión
if (!$conn)
{
    die("Conexión fallida: " . mysqli_connect_error());
}
//echo "Conexión exitosa";
?>

//Mostrar información con inner
 $sql    = "SELECT * FROM tabla1 AS t1 INNER JOIN tabla2 AS t2 ON t1.dato = t2.dato";
 $result = mysqli_query($conn, $sql);

<?php

                $contador = 1;

                while ($fila = mysqli_fetch_assoc($result))
                {
                    $estado = $fila["dato"];

                    if ($estado == 1)
                    {

                        $tipoEstado = "<span class='badge bg-primary'>Activo</span>";

                    }
                    elseif ($estado == 0)
                    {

                        $tipoEstado = "<span class='badge bg-danger'>Inactivo</span>";

                    }

                    ?>

                    <tr>
                        <td>
                            <?php echo $contador; ?>
                        </td>
                        <td>

                            <img width="100" src="<?php echo $fila["dato"]; ?>" alt="">

                        </td>
                        <td>
                            <?php echo $fila["dato"]; ?>
                        </td>
                        <td>
                            <strong>
                                <?php $fila["dato"]; ?>
                            </strong>
                        </td>
                        <td>
                            <?php echo $fila["nombre_categoria"]; ?>
                        </td>
                        <td>
                            <?php echo $tipoEstado; ?>
                        </td>
                        <td>
                            <a href="editar_producto.php?id_producto=<?php echo $fila["dato"] ?>"
                                class="btn btn-warning"><i class="fa-solid fa-pen-to-square"></i></a>

                            <a class="btn btn-danger"
                                href="eliminar_producto.php?id_producto=<?php echo $fila["dato"] ?>"
                                onclick="return confirm('¿Estás seguro de que deseas eliminar este producto?');"><i
                                    class="fa-solid fa-trash"></i></a>
                        </td>
                    </tr>

                    <?php

                    $contador++;

                } ?>

//Agregar 

    if (isset($_POST["dato"]))
    {
        if (isset($_FILES["imagen"]) && $_FILES["imagen"]["error"] == 0)
        {
            $carpeta_imagenes = ""; // Carpeta donde se guardarán las imágenes

            // Genera un nombre único para la imagen utilizando time()
            $nombre_imagen = time();
          
            $nombre_original = $_FILES["imagen"]["name"];

            // Obtener la extensión del archivo
            $extension = pathinfo($nombre_original, PATHINFO_EXTENSION);

            // Generar un nombre de archivo único con extensión
            $imagen = $nombre_imagen . '.' . $extension;

            $destino = $carpeta_imagenes . $imagen;

            // Mueve el archivo temporal al directorio de destino
            if (move_uploaded_file($_FILES["imagen"]["tmp_name"], $destino))
            {
                $imagen = $destino;
            }
            else
            {
                $imagen = "";
            }
        }
        else
        {
            $imagen = "";
        }

        $dato1   = $_POST["dato"];
        ...
	...

        $sql = "INSERT INTO tabla (campos...)
        VALUES ('$dato1')";

        if (mysqli_query($conn, $sql))
        {
            echo '<script>
            window.location = "pagina.php?mensaje=3";
            </script>';
        }
        else
        {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }
        // Cerrar la conexión
        mysqli_close($conn);
    }

//Editar

$id_dato = $_GET["id_dato"];

//seleccionar datos con inner join
     $sql    = "SELECT * FROM tabla1 AS t1 INNER JOIN tabla2 AS t2 ON t1.dato = t2.dato WHERE dato = '$id_dato'";
    $result  = mysqli_query($conn, $sql);
    $fila = mysqli_fetch_array($result);

 //Deben llamar en editar a ller que producto o categoria es y en cada campo en su value poner:
 value="<?php echo $fila["dato"]; ?>" 

if (isset($_POST["dato"]))
    {

        $dato1 = $_POST["dato1"]; 
        

        $sql = "UPDATE tabla SET dato1='$dato1', ...... WHERE id_dato=$id_dato";

        if (mysqli_query($conn, $sql))
        {

            echo '<script>
    window.location = "pagina.php?mensaje=2";
    </script>';
        }
        else
        {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }
        // Cerrar la conexión
        mysqli_close($conn);
    }


//En el formulario deben tener un campo oculto enviando el id

//Para enviar fechas date("Y-m-d")

//Para editar el campo select

<?php if ($fila["id_dato"] == $fila2["id_dato2"]){ ?> selected <?php } ?>
                                   

//Eliminar

if (isset($_GET["dato"]))
{

    $dato = $_GET["dato"];

    // Query DELETE
    $sql = "DELETE FROM tabla WHERE id_dato=$dato";
    if (mysqli_query($conn, $sql))
    {
        echo '<script>

    window.location = "pagina.php?mensaje=1"
    
    </script>';
    }
    else
    {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
    // Cerrar la conexión
    mysqli_close($conn);

}

//Para mostrar mensajes
		if (isset($_GET["mensaje"]))
                    {
                        if ($_GET["mensaje"] == 1)
                        { ?>

                            <div class="alert alert-danger mt-5" role="alert">
                                El producto se eliminó correctamente
                            </div>
//Cerrar if correctamente
