<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Aritmtica</title>
</head>
<body>

<h1>Ejemplo de operaciones aritmeticas en PHP</h1>

<?php
    $a = 8;
    $b = 3;
    echo "<font color=\"red\" size=\"2\"> La suma de $a y $b es : ",$a + $b, "</font><br>";
    echo $a - $b, "<br>";
    echo $a * $b, "<br>";
    echo $a / $b, "<br>";
    $a++;
    echo $a,"<br>";
    $b--;
    echo $b,"<br>";
?>
    
</body>
</html>
