<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bucle While</title>
</head>
<body>
    <?php
        if ( isset( $_POST['number'] )) {
            $number = $_POST['number'];
            $counter = 1;
            while ($counter <= $number) {
                echo "Los bucles son faciles!<br>\n";
                $counter++;
            }
            echo "Terminó el bucle.\n";
        }
    ?>
</body>
</html>