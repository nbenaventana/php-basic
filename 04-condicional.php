<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Condicional</title>
</head>
<body>
    <h1>Condicional IF</h1>
    <?php
        $a = 8;
        $b = 3;
        echo "Valor de a: ",$a," y el valor de b:",$b,"<br>";
        if ($a < $b) {
            echo "a es menor que b";
        }
        else {
            echo " a no es menor que b";
        }

        date_default_timezone_set('America/Argentina/Buenos_Aires');

        $dia=date("d");

        if ($dia<=10){
            echo "sitio activo\n";
        }
        else {
            echo "sitio fuera de servicio\n";
        }
        echo "<br><br> $dia";
        ?>
</body>
</html>