<?php
// Incluir el archivo de conexión
include 'conexion.php';

// Realizar una consulta SQL
$query = "SELECT * FROM tu_tabla"; // Reemplaza 'tu_tabla' con el nombre de tu tabla
$resultado = mysqli_query($conn, $query);

// Chequear si la consulta fue exitosa
if (!$resultado) {
    die("Fallo la consulta: " . mysqli_error($conn));
}

// Recorrer los resultados y mostrarlos
while ($fila = mysqli_fetch_assoc($resultado)) {
    echo "Columna1: " . $fila["nombre_columna1"] . " - Columna2: " . $fila["nombre_columna2"] . "<br>"; // Reemplaza con los nombres de tus columnas
}

// Cerrar la conexión (opcional, ya que se cierra automáticamente al finalizar el script)
mysqli_close($conn);
?>
