<?php 
/**
 * Sabiendo que la funcion RAND nos retorna un valor aleatorio 
 * entre un rango de dos enteros:
 * 
 * $num = rand(1,100);
 * 
 * En la variable $num se almacena un valor entero que la 
 * computadora genera en forma aleatoria entre
 * 1 y 100. Hacer un programa que lo muestre por pantalla al 
 * valor generado. Mostrar ademas si es
 * menor o igual a 50 o si es mayor. Para imprimir el contenido 
 * de una variable tambien utilizamos el
 * 
 * comando echo 
*/


?>